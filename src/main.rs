#[macro_use]
extern crate log;

mod config;
mod countdown_latch;
mod id_generator;
mod messages;
mod routes;
mod socket_server;
mod tmp_storage;

use id_generator::IdGenerator;
use messages::MessageList;
use routes::{messages_list, post_message, AppState};
use socket_server::SocketServer;
use tmp_storage::TmpStorage;

use actix_web::{web, App, HttpServer};

use std::sync::Mutex;

use config::Config;
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::init();

    let config = Config::new();
    let address = format!("{}:{}", config.host, config.port);

    let app_state = web::Data::new(AppState {
        socket_server: Mutex::new(SocketServer::new(config.socket_address)),
        messages_list: Mutex::new(MessageList::default()),
        id_generator: Mutex::new(IdGenerator::new()),
        tmp_storage: Mutex::new(TmpStorage::new(MessageList::default())),
    });

    info!("Server listen on: http://{}", address);
    HttpServer::new(move || {
        App::new()
            .app_data(app_state.clone())
            .service(messages_list)
            .service(post_message)
    })
    .bind(address)?
    .run()
    .await
}
