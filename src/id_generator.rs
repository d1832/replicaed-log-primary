pub struct IdGenerator {
    counter: i32,
}

impl IdGenerator {
    pub fn new() -> Self {
        Self { counter: 0 }
    }

    pub fn generate_next(&mut self) -> i32 {
        self.counter = self.counter + 1;

        self.counter
    }
}
