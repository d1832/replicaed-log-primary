pub struct SocketServer {
    socket: zmq::Socket,
}

impl SocketServer {
    pub fn new(socket_address: String) -> Self {
        let context = zmq::Context::new();
        let socket = context.socket(zmq::REQ).unwrap();

        let connection_result = socket.bind(&socket_address);

        match connection_result {
            Ok(connection) => info!("Server connected and {:?}", connection),
            Err(error) => error!("Error connecting to server {:?}", error),
        }

        Self { socket }
    }

    pub fn send_message(&self, message: String) {
        let message_result = self.socket.send(&message, 0);

        match message_result {
            Ok(result) => info!("Mеssage sent {:?}", result),
            Err(err) => error!("Err message {:?}", err),
        }

        loop {
            let result = self.socket.recv_string(0).unwrap();

            match result {
                Ok(message) => {
                    if message.len() != 0 {
                        info!("reply received: {:?}", message);
                        break;
                    }
                }
                Err(_) => error!("Received message"),
            }
        }
    }
}
