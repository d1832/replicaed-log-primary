use serde::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Message {
    pub text: String,
    pub write_concern: i8,
    pub id: i32,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct JsonMessage {
    pub text: String,
    pub write_concern: i8,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct MessageList {
    messages: Vec<Message>,
}

impl Message {
    pub fn new(text: String, write_concern: i8, id: i32) -> Self {
        Message {
            id,
            text,
            write_concern,
        }
    }

    pub fn as_json(&self) -> String {
        let message_arc = Arc::new(&self);
        let message_copy = Arc::clone(&message_arc);

        let serialized = serde_json::to_string(*message_copy).unwrap();

        println!("Serialized {:?}", serialized);

        serialized
    }
}

impl MessageList {
    pub fn add_message(&mut self, message: Message) {
        self.messages.push(message);
    }
}
