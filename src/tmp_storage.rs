use std::collections::HashMap;

use super::countdown_latch::CountdownLatch;
use super::messages::{Message, MessageList};

#[derive(Debug)]
pub struct TmpStorage {
    pub concerns_map: HashMap<i32, CountdownLatch>,
    pub messages: MessageList,
}

impl TmpStorage {
    pub fn new(messages: MessageList) -> Self {
        Self {
            messages,
            concerns_map: HashMap::new(),
        }
    }

    pub fn add(&mut self, message: Message) {
        self.concerns_map
            .insert(message.id, CountdownLatch::new(message.write_concern));
        self.messages.add_message(message)
    }

    pub fn next_counter(&mut self, id: i32) -> i32 {
        let current_counter = self.concerns_map.get(&id).unwrap();

        current_counter.count_down();
        info!("counter {:?}", current_counter);

        // *self.concerns_map.get_mut(&id).unwrap() = next_value;
        // next_value.counter.count

        0
    }

    pub fn _extract_message(&mut self) {}
}
