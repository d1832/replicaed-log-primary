use actix_web::{get, post, web, Error, HttpRequest, HttpResponse, Responder};
use futures::future::{ready, Ready};
use std::sync::Mutex;

use super::id_generator::IdGenerator;
use super::messages::{JsonMessage, Message, MessageList};
use super::socket_server::SocketServer;
use super::tmp_storage::TmpStorage;

pub struct AppState {
    pub socket_server: Mutex<SocketServer>,
    pub messages_list: Mutex<MessageList>,
    pub id_generator: Mutex<IdGenerator>,
    pub tmp_storage: Mutex<TmpStorage>,
}

impl Responder for MessageList {
    type Error = Error;
    type Future = Ready<Result<HttpResponse, Error>>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self).unwrap();

        ready(Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body)))
    }
}

#[get("/messages")]
pub async fn messages_list(data: web::Data<AppState>) -> impl Responder {
    let messages = data.messages_list.lock().unwrap();

    messages.clone()
}

#[post("/messages")]
pub async fn post_message(
    msg: web::Json<JsonMessage>,
    data: web::Data<AppState>,
) -> impl Responder {
    let mut id_generator = data.id_generator.lock().unwrap();
    let socket_server = data.socket_server.lock().unwrap();

    let message_id = id_generator.generate_next();
    let message = Message::new(msg.text.to_string(), msg.write_concern, message_id);
    let mut messages = data.messages_list.lock().unwrap();
    let mut tmp_storage = data.tmp_storage.lock().unwrap();

    tmp_storage.add(message.clone());
    messages.add_message(message.clone());

    info!("tmp_storage {:?}", tmp_storage);

    tmp_storage.next_counter(message_id);

    socket_server.send_message(message.as_json());

    info!("message {}", message_id);

    HttpResponse::Ok().body("Added success")
}
