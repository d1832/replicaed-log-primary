use std::env;

pub struct Config {
    pub host: String,
    pub port: String,
    pub socket_address: String,
}

impl Config {
    pub fn new() -> Self {
        let host = env::var("HOST")
            .unwrap_or("0.0.0.0".to_string())
            .to_string();
        let port = env::var("PORT").unwrap_or("8000".to_string()).to_string();

        let socket_address = env::var("SOCKET_ADDRESS")
            .unwrap_or("inproc://replicated_log".to_string())
            .to_string();

        Self {
            host,
            port,
            socket_address,
        }
    }
}
