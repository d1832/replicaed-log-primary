use std::sync::Mutex;

#[derive(Debug)]
struct Counter {
    count: i8,
}

impl Counter {
    fn new(count: i8) -> Self {
        Self { count }
    }

    fn decrement(&mut self) {
        self.count = self.count - 1
    }
}

#[derive(Debug)]
pub struct CountdownLatch {
    counter: Mutex<Counter>,
}

impl CountdownLatch {
    pub fn new(initial_count: i8) -> Self {
        Self {
            counter: Mutex::new(Counter::new(initial_count)),
        }
    }

    pub fn count_down(&self) {
        let mut counter = self.counter.lock().unwrap();

        counter.decrement();
    }

    pub fn _is_completed(&self) -> bool {
        let counter = self.counter.lock().unwrap();

        counter.count == 0
    }
}
