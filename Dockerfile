FROM rust:1.48 AS builder

RUN apt-get update \
    && apt-get install -y build-essential libzmq3-dev

WORKDIR /app

COPY . .

RUN cargo build --release


FROM debian:latest

RUN apt-get update \
    && apt-get install -y libzmq3-dev

COPY --from=builder /app/target/release/primary /app/primary

ENV RUST_LOG ${RUST_LOG}
ENV HOST ${HOST}
ENV PORT ${PORT}
ENV SOCKET_PORT ${SOCKET_PORT}

EXPOSE ${PORT} ${SOCKET_PORT}

CMD [ "./app/primary" ]
